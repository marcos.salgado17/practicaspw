from django.shortcuts import render
from apps.Zorros.models import Zorro

# Create your views here.
def Lista(request):
	qs = Zorro.objects.all()
	context = {
		'zorros': qs
	}
	return render(request,'Zorro/lista.html',context)

def Detalles(request,id):
	qs = Zorro.objects.get(id=id)
	context = {
		'zorro': qs
	}
	return render(request,'Zorro/detalles.html',context)