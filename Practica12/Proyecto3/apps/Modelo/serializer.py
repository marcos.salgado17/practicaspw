from rest_framework import serializers
from apps.Triges.models import Tigre

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Tigre
		fields = [
			"nombre",
			"raza",
			"hogar",
			"habitad",
			"color",
			"edad",
			"size",
			"peso",
			"date",

		]
			
			