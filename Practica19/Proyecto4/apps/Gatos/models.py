from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.
class Gato(models.Model):
	nombre = models.CharField(max_length=30)
	raza = models.CharField(max_length=30)
	hogar = models.CharField(max_length=30)	
	color = models.CharField(max_length=30)
	edad = models.IntegerField()
	size = models.FloatField()
	peso = models.FloatField()
	date = models.DateField()

	def __str__(self):
		return self.nombre

	def clean(self,*args,**kwargs):
		raza = self.raza
		raza_list = ["PERSA","BENGALA","AZUL","MUNCHIKIN","SAVANNAH","MANX","COMUN"]
		if raza.upper() not in raza_list:
			raise ValidationError("Tipos de Lobos [Persa,Bengala,Azul,Munchikin,Savannah,Manx,Comun]")
		return super(Gato,self).clean(*args,**kwargs)