from django import forms

from apps.Caballos.models import Caballo

class CaballoForm(forms.ModelForm):
	nombre = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Nombre","class":"form-control"}))
	raza = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Raza","class":"form-control"}))
	hogar = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Raza","class":"form-control"}))
	edad = forms.IntegerField(widget=forms.NumberInput(attrs={"placeholder":"Edad","class":"form-control"}))
	altura = forms.IntegerField(widget=forms.NumberInput(attrs={"placeholder":"Size","class":"form-control"}))
	ancho = forms.IntegerField(widget=forms.NumberInput(attrs={"placeholder":"Size","class":"form-control"}))
	date = forms.DateField(widget=forms.DateInput(attrs={"placeholder":"Registro","class":"form-control"}))
	class Meta:
		model = Caballo
		fields = [
			"nombre",
			"raza",
			"hogar",
			"edad",
			"altura",
			"ancho",
			"date",

		]
			
		