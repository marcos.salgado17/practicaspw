from rest_framework import serializers
from apps.Dragones.models import Dragon

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Dragon
		fields = [
			"nombre",
			"raza",
			"color",
			"edad",
			"peso",
			"date",
		]
			
			