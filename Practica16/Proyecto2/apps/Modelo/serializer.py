from rest_framework import serializers
from apps.Zorros.models import Zorro
from django.contrib.auth.models import User

class ApiUser(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
		]
		
class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Zorro
		fields = [
			"nombre",
			"raza",
			"hogar",
			"habitad",
			"edad",
			"size",
			"date",

		]
			