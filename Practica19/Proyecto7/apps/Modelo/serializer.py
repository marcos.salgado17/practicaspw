from rest_framework import serializers
from apps.Perros.models import Perro
from django.contrib.auth.models import User

class ApiUser(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
		]
			

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Perro
		fields = [
			"nombre",
			"raza",
			"hogar",
			"color",
			"edad",
			"peso",
			"date",
		]
			
			