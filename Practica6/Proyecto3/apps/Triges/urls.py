from django.contrib import admin
from django.urls import path
from apps.Triges.views import Lista,Detalles

app_name='Triges'
urlpatterns = [
    path('lista/',Lista,name='Lista'),
    path('detalles/<int:id>',Detalles,name='Detalles'),
]