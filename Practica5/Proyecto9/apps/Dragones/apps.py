from django.apps import AppConfig


class DragonesConfig(AppConfig):
    name = 'Dragones'
