from rest_framework import serializers
from apps.Perros.models import Perro

class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Perro
		fields = [
			"nombre",
			"raza",
			"hogar",
			"color",
			"edad",
			"peso",
			"date",
		]
			
			