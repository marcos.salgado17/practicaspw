from django.shortcuts import render
from apps.Patos.models import Pato
from apps.Patos.forms import PatoForm

# Create your views here.
def Lista(request):
	qs = Pato.objects.all()
	context ={
		'patos':qs
	}
	return render(request,'Patos/lista.html',context)

def Detalles(request,id):
	qs = Pato.objects.get(id=id)
	context ={
		'pato':qs
	}
	return render(request,'Patos/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = PatoForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Patos:Lista')
	else:
		form = PatoForm()

	return render(request,'Patos/crear.html',{'form':form})