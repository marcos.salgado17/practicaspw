from django.shortcuts import render
from apps.Dragones.models import Dragon
from apps.Dragones.forms import DragonForm
from django.views.generic import ListView,CreateView,DetailView,UpdateView,DeleteView
from django.urls import reverse_lazy
# Create your views here.

#Generics Views
class gLista(ListView):
	model = Dragon
	template_name ='Dragones/glista.html'

class gDetalles(DetailView):
	model  = Dragon
	template_name = 'Dragones/detalles.html'

class gCrear(CreateView):
	model = Dragon
	form_class = DragonForm
	template_name = 'Dragones/crear.html'
	success_url = reverse_lazy('Dragones:gLista')

class gActualizar(UpdateView):
	model = Dragon
	form_class = DragonForm
	template_name = 'Dragones/crear.html'
	success_url = reverse_lazy('Dragones:gLista')

class gEliminar(DeleteView):
	model  = Dragon
	template_name = 'Dragones/geliminar.html'
	success_url = reverse_lazy('Dragones:gLista')
	
#Funciones 
def Lista(request):
	qs = Dragon.objects.all()
	context ={
		'dragones':qs
	}
	return render(request,'Dragones/lista.html',context)

def Detalles(request,id):
	qs = Dragon.objects.get(id=id)
	context ={
		'dragon':qs
	}
	return render(request,'Dragones/detalles.html',context)

def Crear(request):
	if request.method == 'POST':
		form = DragonForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Dragones:Lista')
	else:
		form = DragonForm()

	return render(request,'Dragones/crear.html',{'form':form})

def Actualizar(request,id):
	qs = Dragon.objects.get(id=id)
	if request.method == 'GET':
		form = DragonForm(instance = qs)
	else:
		form = DragonForm(request.POST, instance = qs)
		if form.is_valid():
			form.save()
		return redirect('Dragones:Lista')
	return render(request,'Dragones/crear.html',{'form':form})

def Eliminar(reques,id):
	qs = Dragon.objects.get(id=id)
	qs.delete()
	return redirect('Dragones:Lista')