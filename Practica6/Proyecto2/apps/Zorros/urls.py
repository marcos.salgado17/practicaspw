from django.contrib import admin
from django.urls import path
from apps.Zorros.views import Lista,Detalles

app_name='Zorros'
urlpatterns = [
    path('lista/', Lista, name="Lista"),
    path('detalles/<int:id>', Detalles, name="Detalles"),
]