from django import forms

from apps.Conejos.models import Conejo

class ConejoForm(forms.ModelForm):
	nombre = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Nombre","class":"form-control"}))
	raza = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Raza","class":"form-control"}))
	edad = forms.IntegerField(widget=forms.NumberInput(attrs={"placeholder":"Edad","class":"form-control"}))
	size = forms.IntegerField(widget=forms.NumberInput(attrs={"placeholder":"Size","class":"form-control"}))
	date = forms.DateField(widget=forms.DateInput(attrs={"placeholder":"Registro","class":"form-control"}))
	class Meta:
		model = Conejo
		fields = [
			"nombre",
			"raza",
			"edad",
			"size",
			"date",

		]
			
		