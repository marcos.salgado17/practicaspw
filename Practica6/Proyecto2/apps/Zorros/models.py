from django.db import models

# Create your models here.
class Zorro(models.Model):
	nombre = models.CharField(max_length=30)
	raza = models.CharField(max_length=30)
	hogar = models.CharField(max_length=30)	
	habitad = models.CharField(max_length=30)
	edad = models.IntegerField()
	size = models.FloatField()
	date = models.DateField()

	def __str__(self):
		return self.nombre