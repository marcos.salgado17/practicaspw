from django.contrib import admin
from django.urls import path
from apps.Perros.views import Lista,Detalles

app_name='Perros'
urlpatterns = [
    path('lista/',Lista,name='Lista'),
    path('detalles/<int:id>',Detalles,name='Detalles'),
]