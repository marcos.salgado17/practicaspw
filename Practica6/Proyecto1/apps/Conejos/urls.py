from django.contrib import admin
from django.urls import path,include,re_path
from apps.Conejos.views import Lista,Detalles

app_name='Conejos'
urlpatterns = [
    path('lista/', Lista, name="Lista"),
    path('detalles/<int:id>', Detalles, name="Detalles"),

]
