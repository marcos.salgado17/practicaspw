from rest_framework import serializers
from apps.Caballos.models import Caballo
from django.contrib.auth.models import User

class ApiUser(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
		]
			
class ApiReadOnly(serializers.ModelSerializer):
	class Meta:
		model = Caballo
		fields = [
			"nombre",
			"raza",
			"hogar",
			"edad",
			"altura",
			"ancho",
			"date",

		]
			
			