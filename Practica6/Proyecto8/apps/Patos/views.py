from django.shortcuts import render
from apps.Patos.models import Pato

# Create your views here.
def Lista(request):
	qs = Pato.objects.all()
	context ={
		'patos':qs
	}
	return render(request,'Patos/lista.html',context)

def Detalles(request,id):
	qs = Pato.objects.get(id=id)
	context ={
		'pato':qs
	}
	return render(request,'Patos/detalles.html',context)